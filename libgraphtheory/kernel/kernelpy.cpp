#include "kernelpy.h"

#include "documentwrapper.h"
#include "nodewrapper.h"
#include "edgewrapper.h"
#include "logging_p.h"
#include "kernel/modules/console/consolemodule.h"
#include "libgraphtheory/graphdocument.h"
#include <KLocalizedString>
#include <pybind11/embed.h>
#include <pybind11/stl.h>

using namespace GraphTheory;

class GraphTheory::NodeWrapperPy {
public:
    NodePtr nodePtr;
    GraphDocumentPtr *documentWrapper;
    NodeWrapperPy(NodePtr node, GraphDocumentPtr *documentWrapperPy)
    {
        nodePtr = node;
        documentWrapper = documentWrapperPy;
    }

    ~NodeWrapperPy()
    {

    }

    int id()
    {
        return nodePtr->id();
    }
};

class GraphTheory::EdgeWrapperPy {
public:
    EdgePtr edgePtr;
    GraphDocumentPtr *documentWrapper;
    EdgeWrapperPy(EdgePtr edge, GraphDocumentPtr *documentWrapperPy)
    {
        edgePtr = edge;
        documentWrapper = documentWrapperPy;
    }

    ~EdgeWrapperPy()
    {

    }

    NodeWrapperPy to(){
        return NodeWrapperPy(edgePtr->to(), documentWrapper);
    }

    NodeWrapperPy from(){
        return NodeWrapperPy(edgePtr->from(), documentWrapper);
    }
};

class GraphTheory::DocumentWrapperPy {
public:
    GraphDocumentPtr pyDocument;

    DocumentWrapperPy(GraphDocumentPtr document)
    {
        pyDocument = document;
    }

    ~DocumentWrapperPy()
    {
    }

    uint getObjects()
    {
       return pyDocument->objects(); 
    }

    std::string documentName()
    {
        return pyDocument->documentName().toStdString();
    }

    std::vector<EdgePtr> edges()
    {
        return pyDocument->edges().toStdVector();
    }

    EdgeWrapperPy edge(int i)
    {
        return EdgeWrapperPy(pyDocument->edges()[i].data()->self(), &pyDocument);
    }

    std::vector<NodePtr> nodes()
    {
        return pyDocument->nodes().toStdVector() ;
    }

    GraphDocumentPtr getDocumentPtr()
    {
        return pyDocument;
    }

};

PYBIND11_EMBEDDED_MODULE(wrappers, m) {
    pybind11::class_<GraphDocumentPtr>(m, "GraphDocumentPtr");
    pybind11::class_<GraphTheory::EdgePtr>(m, "EdgePtr");
    pybind11::class_<GraphTheory::Node>(m, "Node");
    pybind11::class_<GraphTheory::NodePtr>(m, "NodePtr");
        //.def("color", &GraphTheory::NodePtr::data().color);
    pybind11::class_<GraphTheory::EdgeList>(m, "EdgeList");
    //    .def("value", &GraphTheory::EdgeList::value);

    pybind11::class_<GraphTheory::DocumentWrapperPy>(m, "GraphDocumentWrapper")
        .def(pybind11::init<GraphDocumentPtr>())
        .def("graphDocumentPtr", &GraphTheory::DocumentWrapperPy::getDocumentPtr)
        .def("objects", &GraphTheory::DocumentWrapperPy::getObjects)
        .def("edges", &GraphTheory::DocumentWrapperPy::edges)
        .def("edge", &GraphTheory::DocumentWrapperPy::edge)
        .def("nodes", &GraphTheory::DocumentWrapperPy::nodes)
        .def("documentName", &GraphTheory::DocumentWrapperPy::documentName);

    pybind11::class_<GraphTheory::EdgeWrapperPy>(m, "EdgeWrapper")
        .def("to", &GraphTheory::EdgeWrapperPy::to)
        .def("fromm", &GraphTheory::EdgeWrapperPy::from);

    pybind11::class_<GraphTheory::NodeWrapperPy>(m, "NodeWrapper")
        .def("id", &GraphTheory::NodeWrapperPy::id);  

}

///BEGIN: Kernel
KernelPy::KernelPy()
{
    
}

KernelPy::~KernelPy()
{

}

void KernelPy::execute(GraphDocumentPtr document, const QString &script)
{
    using namespace pybind11::literals;

    pybind11::scoped_interpreter guard{};
    auto wrappers = pybind11::module_::import("wrappers");
    pybind11::print("======== Script Python ========");
    GraphTheory::DocumentWrapperPy docWrapper = GraphTheory::DocumentWrapperPy(document);
    try{
        auto locals = pybind11::dict("doc"_a= docWrapper);
        pybind11::exec(script.toStdString(), pybind11::globals(), locals);
    }
    catch(...){
        pybind11::print("Parse Error");
    }
    pybind11::print("===============================");
}

void KernelPy::stop()
{
    // TO-DO
}

void KernelPy::processMessage(const QString &messageString, KernelPy::MessageType type)
{
    // TO-DO
}

void KernelPy::attachDebugger()
{
    // TO-DO
}

void KernelPy::detachDebugger()
{
    // TO-DO
}

void KernelPy::triggerInterruptAction()
{
    // TO-DO
}

//END: Kernel
